var rangeInput = document.getElementById("range");
var lightslide = document.getElementsByClassName("lightslide")[0];
    
rangeInput.addEventListener("input", function() {
    lightslide.style.filter = "brightness(" + rangeInput.value + "%)";
});