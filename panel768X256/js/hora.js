var getRemainingTime = (deadline) => {
    var now = new Date(),
      remainTime = (new Date(deadline) - now + 1000) / 1000,
      remainSeconds = ("0" + Math.floor(remainTime % 60)).slice(-2),
      remainMinutes = ("0" + Math.floor((remainTime / 60) % 60)).slice(-2),
      remainHours = ("0" + Math.floor((remainTime / 3600) % 24)).slice(-2),
      remainDays = Math.floor(remainTime / (3600 * 24));
  
    return {
      remainSeconds,
      remainMinutes,
      remainHours,
      remainDays,
      remainTime
    };
  };
  
 
  var countdown = (deadline, finalMessage) => {
    var timerUpdate = setInterval(() => {
      var t = getRemainingTime(deadline);
      $("#clock").html(`<span>${t.remainDays} días </span>${t.remainHours}:${t.remainMinutes}:${t.remainSeconds}`);
      
      if (t.remainTime <= 1) {
        clearInterval(timerUpdate);
        $("#clock").html(finalMessage);
      }
    }, 1000);
  };
  // fecha hasta donde contar || llega el dia y dice: Ya comendo
  countdown("Nov 15 2023 00:00:00 GMT-0300","¡APROVECHA!");
  
 // Cuenta la hora para sacar el banner
  var flimite = (value) => {
    var timerUpdate2 = setInterval(() => {
      var h = getRemainingTime(value);
      console.log(h);
      if (h.remainTime <= 1) {
        clearInterval(timerUpdate2);
        document.getElementById("modalpopup").style.display = "none";
      }
    }, 1000);
    
  };
  
  // flimite("Nov 22 2022 09:37:00 GMT-0300");
  flimite("Nov 18 2023 23:59:00 GMT-0300");
  // flimite("Nov 28 2022 23:59:00 GMT-0300");