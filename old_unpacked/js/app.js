var angX = 0;
var angY = 0;

$('.boton').on('click', function() {
  switch ($(this).attr("id")) {
    case "arriba":
      angX = angX + 45;
      break;
    case "abajo":
      angX = angX - 45;
      break;
    case "derecha":
      angY = angY + 45;
      break;
    case "izquierda":
      angY = angY - 45;
      break;
  }
  $('#cube').attr('style', 'transform: rotateX(' + angX + 'deg) rotateY(' + angY + 'deg);')
});